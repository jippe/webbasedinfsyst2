package nl.bioinf.zaverstappen.SpeciesBrowser.models;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

public class CSVReader {

    public static Map<String, Cat> fileReader(String paths) {
        Path path = Paths.get(paths);
        //Create a hashmap to store cats and access them by EnglishName
        Map<String, Cat> cats = new HashMap<>();

        try (BufferedReader reader = Files.newBufferedReader(path)) {
            String line;
            int lineNumber = 0;
            while ((line = reader.readLine()) != null) {
                lineNumber++;
                //Skip first line(header)
                if(lineNumber == 1) continue;

                String[] elements =  line.split(";");

                //Fetch elements of a cat
                String scientificName = elements[0];
                String englishName = elements[1];
                String dutchName = elements[2];
                Integer weight = Integer.parseInt(elements[3]);
                Integer size = Integer.parseInt(elements[4]);
                String picture = elements[5];

                //Create new Cat instance for every cat in file
                Cat cat = new Cat(scientificName, englishName, dutchName, weight, size, picture);
                //Add cat to hashmap
                cats.put(cat.getScientificName(), cat);
            }
        } catch (
                IOException e) {
            e.printStackTrace();
        }
        return cats;
    }
}
