package nl.bioinf.zaverstappen.Welcome_Project.models;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PhraseFactoryTest {

    @Test
    void getPhrase() {
        for (int i=0; i<50; i++) {
            int random = Integer.parseInt(PhraseFactory.getPhrase("bullshit"));
            assertTrue(random >= 1 && random <= PhraseFactory.MAX_PHRASE_COUNT);
        }
        for (int i=0; i<50; i++) {
            int random = Integer.parseInt(PhraseFactory.getPhrase("management"));
            assertTrue(random >= 1 && random <= PhraseFactory.MAX_PHRASE_COUNT);
        }
    }
}