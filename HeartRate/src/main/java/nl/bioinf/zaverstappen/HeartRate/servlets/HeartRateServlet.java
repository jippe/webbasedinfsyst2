package nl.bioinf.zaverstappen.HeartRate.servlets;
import nl.bioinf.zaverstappen.HeartRate.config.WebConfig;
import nl.bioinf.zaverstappen.HeartRate.models.ZoneDeterminer;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.http.HttpRequest;
import java.util.List;
import java.util.Locale;

@WebServlet(name = "HeartRateServlet", urlPatterns = "/heart_rate_zones")
public class HeartRateServlet extends HttpServlet {
    private TemplateEngine templateEngine;

    @Override
    public void init() throws ServletException {
        this.templateEngine = WebConfig.createTemplateEngine(getServletContext());
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Locale locale = request.getLocale();
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                locale);
        //ctx.setVariable("error_msg", "");
        templateEngine.process("HeartRateZones", ctx, response.getWriter());
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String heartRate = request.getParameter("max_heart_rate");
        Locale locale = request.getLocale();
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                locale);
        if(heartRate.length() != 0){
            final List<Integer> zones = ZoneDeterminer.determineHeartRateZones(Integer.parseInt(heartRate));
            ctx.setVariable("heartrate", heartRate);
            ctx.setVariable("zone_50", zones.get(0));
            ctx.setVariable("zone_60", zones.get(1));
            ctx.setVariable("zone_70", zones.get(2));
            ctx.setVariable("zone_80", zones.get(3));
            ctx.setVariable("zone_90", zones.get(4));
            templateEngine.process("DeterminedZones", ctx, response.getWriter());
        } else {
            //ctx.setVariable("error_msg", "#{error.msg}");
            templateEngine.process("HeartRateZones", ctx, response.getWriter());
        }

    }
}
