package nl.bioinf.zaverstappen.HeartRate.models;

import java.util.ArrayList;
import java.util.List;

public class ZoneDeterminer {

    public static List<Integer> determineHeartRateZones(int heartRate){
        List<Integer> hrZones = new ArrayList<>();
        hrZones.add((int)(heartRate * 0.5));
        hrZones.add((int)(heartRate * 0.6));
        hrZones.add((int)(heartRate * 0.7));
        hrZones.add((int)(heartRate * 0.8));
        hrZones.add((int)(heartRate * 0.9));
        return hrZones;
    }

}
